# MWPS-merge-trains

## Use-case 1 -  `Pipeline for merged results` is enable and `pipeline must succeed` is checked.

![Screen_Shot_2021-05-17_at_6.27.52_PM](/uploads/876bf155ad9d83182eb80cf3b30ec81b/Screen_Shot_2021-05-17_at_6.27.52_PM.png)

-------
-------

## Use-case 2 -  `Merge trains` is enable and `pipeline must succeed` is checked.

![Screen_Shot_2021-05-17_at_6.24.28_PM](/uploads/5aa50a605dbb6892eb03cb6ad04b8712/Screen_Shot_2021-05-17_at_6.24.28_PM.png)
